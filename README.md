ZK Atlantic based theme for ampere
=================================


Forked from Anozi Mada's idempiere theme https://github.com/anozimada/atlantic

Theme modified for ampere, deployed as folder (jar packaging not currently used).

Compiles theme less files using:

https://github.com/zkoss/zkless-engine

Install zkless-engine from npm. :

```
npm install zkless-engine --save-dev
```

Basic compile command specifying source and output directories (see zkless-engine readme for info on parameters):

```
npx zklessc --source src/archive/web --output target/classes/web/mytheme
```

Or modify package.json script paths and execute with:

```
npm run zklessc
```

or to watch and compile on change:


```
npm run zklessc-dev
```
